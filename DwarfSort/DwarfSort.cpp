﻿#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <omp.h>

size_t n = 5000; 
long* arr = NULL; 
long* arrP = NULL; 
long* arrM = NULL;
int p;
int m;


void Read()
{
	size_t i;
	arr = (long*)calloc(n, sizeof(long));
	arrM = (long*)calloc(n/2, sizeof(long));
	arrP = (long*)calloc(n/2+1, sizeof(long));
	//printf("Array: ");
	for (int i = 0; i < n; i++)
		arr[i] = rand() % 200 - 100;
	
	
}

void Sort(int j, long MyArr[], size_t MyN)
{
	size_t i = 1;

	while (i < MyN/*если мы не в конце*/)
	{
		if (i == 0) {
			i = 1;
		}
		if (MyArr[i - 1] <= MyArr[i]) {
			++i; // идём вперед
		}
		else {
			// меняем местами
			long tmp = MyArr[i];
			MyArr[i] = MyArr[i - 1];
			MyArr[i - 1] = tmp;
			// идём назад
			--i;
		}
	}

	

}


void DivideArr(long MyArr[], int MyN)
{
	 p = 0;
	 m = 0;
	for (int i = 0; i < MyN; i++)
	{
		if (MyArr[i]<=0)
		{
			arrM[m] = MyArr[i];
			m++;
		}
		else
		{
			arrP[p] = MyArr[i];
			p++;
		}

	}
	//std::cout << "Size array ArrM = " << m << "\n" << "Size array ArrP = " << p<< "\n";
	
}


void MergeArr(long MyArr[], int MyN)
{
	int k;
	for (size_t i = 0; i < m; i++)
	{
		arr[i] = arrM[i];
		k = i;
	}

	for (size_t i = 0; i < n; i++)
	{
		arr[m]= arrP[i];
		m++;
	}
}

void Write(long MyArr[], size_t MyN)
{
	size_t i;

	printf("Result: ");
	for (i = 0; i < MyN; i++) {
		printf("%d ", MyArr[i]);
	}
	printf("\n");
}

void main()
{
	system("color F2");
	double  oneTime;

	
	std::cout << "\n";
	for (int j = 1; j <= 2; j++)
	{
		Read();
		//Write(arr, n);
		
		
		double time = omp_get_wtime();


		

		#pragma omp parallel if (j ==2)
		if (omp_in_parallel())
		{
			DivideArr(arr, n);

			#pragma omp parallel sections num_threads(j)
			{

				#pragma omp section
				{
					Sort(j, arrM, m);
				}
				#pragma omp section
				{
					Sort(j, arrP, p);
				}
			}

			
			
		}
		else
		{
			Sort(j, arr, n);
			//Write(arr, n);
		}

		if (j==2)
		{
			//Write(arrM, m);
			//Write(arrP, p);

			MergeArr(arr,n);
			//Write(arr, n);
		}
		double time1 = omp_get_wtime();
		if (j == 1)
		{
			oneTime = time1 - time;
		}
		double thisTime = time1 - time;
		printf("threads = %d\ttime = %f acc = %f\n", j, thisTime, oneTime / thisTime);
		std::cout << "\n";
		
		
		
	}
	
	
	delete arr;
	delete arrM;
	delete arrP;

}



